var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.insert = function(params, callback) {

    // insert the publisher
    var query = 'INSERT INTO publisher (publisher_name, country, size) VALUES (?)';

    var queryData = [params.publisher_name, params.country, params.size];

    connection.query(query, [queryData], function(err, result) {
        callback(err, result);
    });
};


exports.getAll = function(callback) {
    var query = 'SELECT * FROM publisher';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(publisher_name, callback) {
    var query = 'SELECT * FROM publisher WHERE publisher_name = ?';
    var queryData = [publisher_name];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};


exports.update = function(params, callback) {
    var query = 'UPDATE publisher SET publisher_name = ?, country = ?, size = ?,  WHERE publisher_name = ?';
    var queryData = [params.publisher_name, params.country, params.size, params.publisher_name];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};

exports.delete = function(publisher_name, callback) {
    var query = 'DELETE FROM publisher WHERE publisher_name = ?';
    var queryData = [publisher_name];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};


exports.edit = function(publisher_name, callback) {
    var query = 'SELECT * FROM publisher WHERE publisher_name = ?';
    var queryData = [publisher_name];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
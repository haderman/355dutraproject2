var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.insert = function(params, callback) {

    // insert the game
    var query = 'INSERT INTO game (game_title, esrb_rating, genre, release_date) VALUES (?)';

    var queryData = [params.game_title, params.esrb_rating, params.genre,params.release_date];

    connection.query(query, [queryData], function(err, result) {
        callback(err, result);
    });
};


exports.getAll = function(callback) {
    var query = 'SELECT * FROM game';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(game_title, callback) {
    var query = 'SELECT * FROM game WHERE game_title = ?';
    var queryData = [game_title];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};


exports.update = function(params, callback) {
    var query = 'UPDATE game SET game_title = ?, esrb_rating = ?, genre = ?, release_date = ? WHERE game_title = ?';
    var queryData = [params.game_title, params.esrb_rating, params.genre, params.release_date,params.game_title];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};

exports.delete = function(game_title, callback) {
    var query = 'DELETE FROM game WHERE game_title = ?';
    var queryData = [game_title];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};


exports.edit = function(game_title, callback) {
    var query = 'SELECT * FROM game WHERE game_title = ?';
    var queryData = [game_title];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
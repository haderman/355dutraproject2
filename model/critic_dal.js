var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.insert = function(critic_name, callback) {

    // insert the critic
    var query = 'INSERT INTO critic (critic_Name) VALUES (?)';

    var queryData = [critic_name];

    connection.query(query, [queryData], function(err, result) {
        callback(err, result);
    });
};


exports.getAll = function(callback) {
    var query = 'SELECT * FROM critic';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(critic_name, callback) {
    var query = 'SELECT * FROM critic WHERE critic_name = ?';
    var queryData = [critic_name];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};


exports.update = function(critic_name, callback) {
    var query = 'UPDATE critic SET critic_name = ?   WHERE critic_name = ?';
    var queryData = [critic_name];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};

exports.delete = function(critic_name, callback) {
    var query = 'DELETE FROM critic WHERE critic_name = ?';
    var queryData = [critic_name];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};


exports.edit = function(critic_name, callback) {
    var query = 'SELECT * FROM critic WHERE critic_name = ?';
    var queryData = [critic_name];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
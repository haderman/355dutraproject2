var express = require('express');
var router = express.Router();
var studio_dal = require('../model/studio_dal');


router.get('/all', function(req, res) {
    studio_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('studio/studioViewAll', { 'result':result });
        }
    });

});

// View the company for the given id
router.get('/', function(req, res){
    if(req.query.studio_name == null) {
        res.send('studio_name is null');
    }
    else {
        studio_dal.getById(req.query.studio_name, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('studio/studioViewById', {'result': result});
            }
        });
    }
});


router.get('/add', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    studio_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('studio/studioAdd', {'studio': result});
        }
    });
});



router.get('/insert', function(req, res){
    // simple validation
    if(req.query.studio_name == "") {
        res.send('Please provide a studio name.');
    }
    else if(req.query.country == "") {
        res.send('Please provide a country.');
    }
    else if(req.query.studio == "") {
        res.send('Please provide a studio number.');
    }

    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        studio_dal.insert(req.query, function(err,result) {
            if (err) {
                console.log(err)
                res.send(err);
            }
            else {
                studio_dal.getAll(function(err, result){
                    if(err) {
                        res.send(err);
                    }
                    else {
                        res.render('studio/studioViewAll', { 'result':result, was_successful: true });
                    }
                });
            }
        });
    }
});

router.get('/edit', function(req, res){
    if(req.query.studio_name == null) {
        res.send('A studio name is required.');
    }
    else {
        studio_dal.edit(req.query.studio_name, function(err, result){
            res.render('studio/studioUpdate', {studio: result[0]});
        });
    }
});



router.get('/update', function(req, res) {
    studio_dal.update(req.query, function(err, result){
        res.redirect(302, '/studio/all');
    });
});


router.get('/delete', function(req, res){
    if(req.query.studio_name == null) {
        res.send('studio_name is null');
    }
    else {
        studio_dal.delete(req.query.studio_name, function(err, result){
            if(err) {
                res.send(err);
            }
            else {
                //poor practice, but we will handle it differently once we start using Ajax
                res.redirect(302, '/studio/all');
            }
        });
    }
});

module.exports = router;

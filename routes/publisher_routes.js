var express = require('express');
var router = express.Router();
var publisher_dal = require('../model/publisher_dal');


router.get('/all', function(req, res) {
    publisher_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('publisher/publisherViewAll', { 'result':result });
        }
    });

});

// View the company for the given id
router.get('/', function(req, res){
    if(req.query.publisher_name == null) {
        res.send('publisher_name is null');
    }
    else {
        publisher_dal.getById(req.query.publisher_name, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('publisher/publisherViewById', {'result': result});
            }
        });
    }
});


router.get('/add', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    publisher_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('publisher/publisherAdd', {'publisher': result});
        }
    });
});




router.get('/insert', function(req, res){
    // simple validation
    if(req.query.publisher_name == "") {
        res.send('Please provide a publisher name.');
    }
    else if(req.query.country == "") {
        res.send('Please provide a country.');
    }
    else if(req.query.size == "") {
        res.send('Please provide a size');
    }

    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        publisher_dal.insert(req.query, function(err,result) {
            if (err) {
                console.log(err)
                res.send(err);
            }
            else {
                publisher_dal.getAll(function(err, result){
                    if(err) {
                        res.send(err);
                    }
                    else {
                        res.render('publisher/publisherViewAll', { 'result':result, was_successful: true });
                    }
                });
            }
        });
    }
});

router.get('/edit', function(req, res){
    if(req.query.publisher_name == null) {
        res.send('A publisher name is required.');
    }
    else {
        publisher_dal.edit(req.query.publisher_name, function(err, result){
            res.render('publisher/publisherUpdate', {publisher: result[0]});
        });
    }
});



router.get('/update', function(req, res) {
    publisher_dal.update(req.query, function(err, result){
        res.redirect(302, '/publisher/all');
    });
});


router.get('/delete', function(req, res){
    if(req.query.publisher_name == null) {
        res.send('publisher_name is null');
    }
    else {
        publisher_dal.delete(req.query.publisher_name, function(err, result){
            if(err) {
                res.send(err);
            }
            else {
                //poor practice, but we will handle it differently once we start using Ajax
                res.redirect(302, '/publisher/all');
            }
        });
    }
});

module.exports = router;

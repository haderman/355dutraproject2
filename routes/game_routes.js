var express = require('express');
var router = express.Router();
var game_dal = require('../model/game_dal');


router.get('/all', function(req, res) {
    game_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('game/gameViewAll', { 'result':result });
        }
    });

});

// View the company for the given id
router.get('/', function(req, res){
    if(req.query.game_title == null) {
        res.send('game_title is null');
    }
    else {
       game_dal.getById(req.query.game_title, function(err,result) {
           if (err) {
               res.send(err);
           }
           else {
               res.render('game/gameViewById', {'result': result});
           }
        });
    }
});


router.get('/add', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    game_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('game/gameAdd', {'game': result});
        }
    });
});




router.get('/insert', function(req, res){
    // simple validation
    if(req.query.game_title == "") {
        res.send('Please provide a game title.');
    }
    else if(req.query.esrb_rating == "") {
        res.send('Please provide a esrb rating.');
    }
    else if(req.query.genre == "") {
        res.send('Please provide a genre');
    }
    else if(req.query.release_date == "") {
        res.send('Please provide a release date.');
    }
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        game_dal.insert(req.query, function(err,result) {
            if (err) {
                console.log(err)
                res.send(err);
            }
            else {
                game_dal.getAll(function(err, result){
                    if(err) {
                        res.send(err);
                    }
                    else {
                        res.render('game/gameViewAll', { 'result':result, was_successful: true });
                    }
                });
            }
        });
    }
});

router.get('/edit', function(req, res){
    if(req.query.game_title == null) {
        res.send('A game title is required.');
    }
    else {
        game_dal.edit(req.query.game_title, function(err, result){
            res.render('game/gameUpdate', {game: result[0]});
        });
    }
});



router.get('/update', function(req, res) {
    game_dal.update(req.query, function(err, result){
       res.redirect(302, '/game/all');
    });
});


router.get('/delete', function(req, res){
    if(req.query.game_title == null) {
        res.send('game_title is null');
    }
    else {
         game_dal.delete(req.query.game_title, function(err, result){
             if(err) {
                 res.send(err);
             }
             else {
                 //poor practice, but we will handle it differently once we start using Ajax
                 res.redirect(302, '/game/all');
             }
         });
    }
});

module.exports = router;
